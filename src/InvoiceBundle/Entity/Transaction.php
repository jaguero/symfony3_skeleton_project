<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Core\Transaction as BaseTransaction;
use Flowcode\FinancialBundle\Model\Core\TransactionInterface;

/**
 * Transaction
 *
 * @ORM\Table(name="invoice_transaction")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\TransactionRepository")
 */
class Transaction extends BaseTransaction implements TransactionInterface
{

}

