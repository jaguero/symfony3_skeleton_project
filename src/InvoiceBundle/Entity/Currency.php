<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Currency\Currency as BaseCurrency;
use Flowcode\FinancialBundle\Model\Currency\CurrencyInterface;

/**
 * Currency
 *
 * @ORM\Table(name="invoice_currency")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\CurrencyRepository")
 */
class Currency extends BaseCurrency implements CurrencyInterface
{

}

