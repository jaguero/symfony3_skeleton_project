<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Document\DocumentItem as BaseDocumentItem;
use Flowcode\FinancialBundle\Model\Document\DocumentItemInterface;

/**
 * DocumentItem
 *
 * @ORM\Table(name="document_item")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\DocumentItemRepository")
 */
class DocumentItem extends BaseDocumentItem implements DocumentItemInterface
{

}

