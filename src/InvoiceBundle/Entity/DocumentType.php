<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Document\DocumentType as BaseDocumentType;
use Flowcode\FinancialBundle\Model\Document\DocumentTypeInterface;

/**
 * DocumentType
 *
 * @ORM\Table(name="document_type")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\DocumentTypeRepository")
 */
class DocumentType extends BaseDocumentType implements DocumentTypeInterface
{

}

