<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Document\Document as BaseDocument;
use Flowcode\FinancialBundle\Model\Document\DocumentInterface;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\DocumentRepository")
 */
class Document extends BaseDocument implements DocumentInterface
{

}

