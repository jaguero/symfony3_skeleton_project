<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Payment\Expense as BaseExpense;
use Flowcode\FinancialBundle\Model\Payment\ExpenseInterface;

/**
 * Expense
 *
 * @ORM\Table(name="invoice_expense")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\ExpenseRepository")
 */
class Expense extends BaseExpense implements ExpenseInterface
{

}

