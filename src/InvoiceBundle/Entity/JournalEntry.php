<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Core\JournalEntry as BaseJournalEntry;
use Flowcode\FinancialBundle\Model\Core\JournalEntryInterface;

/**
 * JournalEntry
 *
 * @ORM\Table(name="invoice_journal_entry")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\JournalEntryRepository")
 */
class JournalEntry extends BaseJournalEntry implements JournalEntryInterface
{

}

