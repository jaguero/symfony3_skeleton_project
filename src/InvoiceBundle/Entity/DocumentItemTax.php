<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Document\DocumentItemTax as BaseDocumentItemTax;
use Flowcode\FinancialBundle\Model\Document\DocumentItemTaxInterface;

/**
 * DocumentItemTax
 *
 * @ORM\Table(name="document_item_tax")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\DocumentItemTaxRepository")
 */
class DocumentItemTax extends BaseDocumentItemTax implements DocumentItemTaxInterface
{

}

