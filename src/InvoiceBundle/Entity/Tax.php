<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Tax as BaseTax;
use Flowcode\FinancialBundle\Model\TaxInterface;

/**
 * Tax
 *
 * @ORM\Table(name="invoice_tax")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\TaxRepository")
 */
class Tax extends BaseTax implements TaxInterface
{

}

