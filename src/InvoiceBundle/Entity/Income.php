<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Payment\Income as BaseIncome;
use Flowcode\FinancialBundle\Model\Payment\IncomeInterface;

/**
 * Income
 *
 * @ORM\Table(name="invoice_income")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\IncomeRepository")
 */
class Income extends BaseIncome implements IncomeInterface
{

}

