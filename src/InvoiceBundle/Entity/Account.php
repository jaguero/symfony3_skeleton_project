<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flowcode\FinancialBundle\Entity\Core\Account as BaseAccount;
use Flowcode\FinancialBundle\Model\Core\AccountInterface;

/**
 * Account
 *
 * @ORM\Table(name="invoice_account")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\AccountRepository")
 */
class Account extends BaseAccount implements AccountInterface
{

}

